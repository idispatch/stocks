ifeq ($(OS),Windows_NT)
PYTHON:=c:\\Python33\\python.exe
else
PYTHON:=python3
endif

CSV_FILES:=$(filter-out all.csv,$(wildcard *.csv))

all: all.json

all.csv: $(CSV_FILES)
	cat $^ > $@

all.json: all.csv statements.json
	$(PYTHON) ./stocks.py -m -o $@ $^

clean:
	rm -f all.json all.csv

.PHONY: statement summary profit commission dump 2009 2010 2011 2012 2013 2014

statement: all.json
	$(PYTHON) ./stocks.py --state

summary: all.json
	$(PYTHON) ./stocks.py -c all.json | grep Symbol: | sort

dump: all.json
	$(PYTHON) ./stocks.py -d all.json

commission: all.json
	$(PYTHON) ./stocks.py --commission -b 2009.1.1 -e 2009.12.31 all.json
	$(PYTHON) ./stocks.py --commission -b 2010.1.1 -e 2010.12.31 all.json
	$(PYTHON) ./stocks.py --commission -b 2011.1.1 -e 2011.12.31 all.json
	$(PYTHON) ./stocks.py --commission -b 2012.1.1 -e 2012.12.31 all.json
	$(PYTHON) ./stocks.py --commission -b 2013.1.1 -e 2013.12.31 all.json

profit:
	$(PYTHON) ./stocks.py -c all.json

2009: all.json
	$(PYTHON) ./stocks.py -c -b 2009.1.1 -e 2009.12.31 $^

2010: all.json
	$(PYTHON) ./stocks.py -c -b 2010.1.1 -e 2010.12.31 $^

2011: all.json
	$(PYTHON) ./stocks.py -c -b 2011.1.1 -e 2011.12.31 $^

2012: all.json
	$(PYTHON) ./stocks.py -c -b 2012.1.1 -e 2012.12.31 $^

2013: all.json
	$(PYTHON) ./stocks.py -c -b 2013.1.1 -e 2013.12.31 $^

2014: all.json
	$(PYTHON) ./stocks.py -c -b 2014.1.1 -e 2014.12.31 $^

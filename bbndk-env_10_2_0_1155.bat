@echo off
set BASE_DIR=%~dp0
set BASE_DIR_REPLACED=%BASE_DIR:\=/%
for /f "delims=" %%x in ('dir /od /b %BASE_DIR%\features\com.qnx.tools.jre.win32_*') do set LATEST_JRE=%%x
set QNX_HOST=%BASE_DIR_REPLACED%/host_10_2_0_15/win32/x86
set QNX_TARGET=%BASE_DIR_REPLACED%/target_10_2_0_1155/qnx6
set QNX_CONFIGURATION=%HOMEDRIVE%%HOMEPATH%\AppData\Local\Research In Motion\BlackBerry Native SDK
set MAKEFLAGS=-I%QNX_TARGET%/usr/include
set PATH=%BASE_DIR%\host_10_2_0_15\win32\x86\usr\bin;%QNX_CONFIGURATION%\bin;%BASE_DIR%\features\%LATEST_JRE%\jre\bin;%BASE_DIR%\host_10_2_0_15\win32\x86\usr\python32;%BASE_DIR%\host_10_2_0_15\win32\x86\usr\python32\Scripts;%PATH%
set PYTHONPATH=
set CPUVARDIR=armle-v7
set QDE=%BASE_DIR%

call %BASE_DIR%\displayvars.bat
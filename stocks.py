#!/usr/local/bin/python3

import os
import sys
import re
import datetime
import pprint
import getopt
import json
import locale

def datetime_to_string(d):
    '''Converts datetime.datetime object to string'''
    return "%(year)s/%(month)s/%(day)s" % d

def string_to_datetime(s):
    '''Converts string to datetime.datetime object'''
    result = re.split('[./:-]', s)
    if not result:
        return None
    year = int(result[0])
    month = int(result[1]) if len(result) > 1 else 1
    day = int(result[2]) if len(result) > 2 else 1
    return datetime.date(year, month, day)

def to_currency(value):
    return locale.currency(value, grouping=True)

def convert_dict_value(p, attribute_name, function):
    if attribute_name in p:
        p[attribute_name] = function(p[attribute_name])

def convert_dict_values(p, attribute_names, function):
    if type(p) == dict:
        [convert_dict_value(p, name, function) for name in attribute_names]
        [convert_dict_values(x, attribute_names, function) for x in p.values()]
    elif type(p) == list:
        [convert_dict_values(x, attribute_names, function) for x in p]
    elif type(p) == tuple:
        raise Exception('Tuples are not supported')
    return p

def convert_datetime(p, attribute_names):
    '''Converts values of attributes with names in attribute_names in object p to datetime.datetime'''
    return convert_dict_values(p, attribute_names, string_to_datetime)

def parse_symbols(symbols):
    '''symbols -- a string, list of symbol names
    returns a list of symbols
    '''
    return [s.upper() for s in re.split('[,:;\\s]', symbols)] if symbols else []

def in_daterange(range, date):
    if not range or len(range) != 2:
        return True
    if range[0] is not None and date < range[0]:
        return False
    if range[1] is not None and date > range[1]:
        return False
    return True

def print_date_filter(date_filter):
    if not date_filter or len(date_filter) < 2:
        return
    if date_filter[0] or date_filter[1]:
        begin, end = date_filter
        begin = ("From %s" % begin) if begin else ''
        end = ("To %s" % end) if end else ''
        print("Date Filter: %s" % ' '.join((begin, end)))

def print_symbol_filter(symbol_filter):
    if symbol_filter: print("Symbol filter: %s" % symbol_filter)

class Table(object):
    Formats = {
        "date":         {'name':'Date', 'width':10, 'align':'<', 'padding':1, 'conversion': str},
        "amount":       {'name':'Amount', 'width':12, 'align':'>', 'conversion': to_currency},
        "total":        {'name':'Total', 'width':12, 'align':'>', 'conversion': to_currency},
        "currency":     {'name':'Currency', 'width':8, 'align':'^'},
        "account":      {'name':'Account', 'width':16, 'align':'<'},
        "symbol":       {'name':'Symbol', 'width':8, 'align':'^'},
        "action":       {'name':'Action', 'width':8, 'align':'^'},
        "count":        {'name':'Count', 'width':8, 'align':'>', 'padding':1},
        "description":  {'name':'Description', 'width':50, 'align': '<'},
        "state":        {'name':'State', 'width':60, 'align':'<', 'padding':1},
        "name":         {'name':'Name', 'width':16, 'align': '<'},
    }
    def __init__(self, columns):
        self._columns = columns
        self._formats = []
        self._line = '+'
        for c in columns:
            padding = c.get('padding', 0)
            name = c.get('name','')
            width = c.get('width', len(name))
            x = '-' * (width + padding * 2)
            self._line += x
            self._line += '+'
        self.header()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.footer()

    def __call__(self, *args):
        self.row(*args)

    def row(self, *args):
        columns = []
        for i, f in enumerate(self._formats):
            padding = f.get('padding', 0)
            padding_str = ' '*padding
            conversion = f.get('conversion', 0)
            text = '{1}{0: {align}{width}{format}}{1}'.format(conversion(args[i]), padding_str, **f)
            width = f.get('width', 0)
            if len(text) > width + padding * 2:
                text = text[:width + padding * 2]
            columns.append(text)
        line = '|' + '|'.join(columns) + '|'
        print(line)

    def header(self):
        self.line()
        self._formats = []
        h = []
        for c in self._columns:
            format=c.get('format', '')
            name = c.get('name','')
            width = c.get('width', len(name))
            align = c.get('align', '^')
            padding = c.get('padding', 0)
            conversion = c.get('conversion', lambda x: x)
            f = {'format':format,
                 'name':name,
                 'width':width,
                 'align':align,
                 'padding':padding,
                 'conversion':conversion}
            self._formats.append(f)
            text = '{0: {align}{width}{format}}'.format(name, **f)
            text = ' '*padding + text + ' '*padding
            h.append(text)
        h = '|' + '|'.join(h) + '|'
        print(h)
        self.line()

    def footer(self):
        self.line()

    def line(self):
        print(self._line)

def read_csv_file(fname):
    try:
        lines = open(fname, mode='r', newline=None).readlines()
        all_trades = []

        lineNumber = 0
        for currentLine in lines:
            lineNumber += 1
            fields = []
            t = ''
            i = 0

            currentLine = currentLine.strip()
            if currentLine and currentLine[0] == '#':
                continue

            while i < len(currentLine):
                c = currentLine[i]
                if c == ',':
                    fields.append(t)
                    t=''
                    i += 1
                elif c == '"':
                    i += 1
                    for z in currentLine[i:]:
                        if z == '"':
                            i += 1
                            break
                        else:
                            t += z
                            i += 1
                else:
                    t += c
                    i += 1
            if t:
                fields.append(t)

            for i in range(len(fields)):
                if not fields[i]:
                    continue
                if i in (1,2): # must be date
                    m = re.search('(\\d\\d)/(\\d\\d)/(\\d\\d)', fields[i])
                    if m:
                        fields[i] = datetime.date(int(m.group(3)) + 2000,
                                             int(m.group(2)),
                                             int(m.group(1)))
                    else:
                        print('Error processing line %d: %s' % (lineNumber, currentLine))
                        print('Field[%d]: ' % (i, fields[i]))
                        raise Exception()
                elif i in (5,10,11,12,13,14,15,16):
                    if fields[i][0] == '(' and fields[i][len(fields[i]) - 1] == ')':
                        fields[i] = '-' + fields[i][1:len(fields[i]) - 1]
                    try:
                        d = float(fields[i].replace(',',''))
                        fields[i] = d
                    except:
                        pass

            fields[3] = "%s-%s-%s" % (fields[6], fields[3], fields[1])
            all_trades.append({'file-name': fname,
                               'account': fields[0],
                               'trade-date': fields[1],
                               'settlement-date': fields[2],
                               'trade-id': fields[3],
                               'action': fields[4],
                               'quantity': fields[5],
                               'symbol': fields[6],
                               'description': fields[7],
                               'tb': fields[8],
                               'exchange': fields[9],
                               'price': fields[10],
                               'gross-amount': fields[11],
                               'commission': fields[12],
                               'fee-amount': fields[13],
                               'interest-amount': fields[14],
                               'net-amount': fields[15],
                               'net-amount-currency': fields[16]})
        print("Total trades in file '%s': %s" % (fname, len(all_trades)))
        return all_trades
    except Exception as err:
        print("Error reading file %s: %s" % (fname, str(err)))
        sys.exit(1)

def read_trades_from_json(fname):
    contents = open(fname, 'r').read()
    all_trades = json.loads(contents)
    for trade in all_trades:
        trade['trade-date'] = string_to_datetime(trade['trade-date'])
        trade['settlement-date'] = string_to_datetime(trade['settlement-date'])
    return all_trades

def calculate(fname, symbol_filter, date_filter):
    accounts = {}
    symbols = {}
    trades_by_stock = {}
    trades_by_date = []

    if type(fname) == list:
        fname = fname[0]

    all_trades = read_trades_from_json(fname)
    trades_by_date = list(sorted(all_trades,
                                 key=lambda x: x['trade-date']))
    trades_by_date.reverse() # most recent - first

    # all_trades = list of dictionaries
    for trade in all_trades:
        account = trade['account']
        symbol = trade['symbol']
        trade_id = trade['trade-id']
        trade_date = trade['trade-date']

        if not in_daterange(date_filter, trade_date):
            continue

        # if symbol_filter is set and symbol_filter not equal to current symbol - skip
        if symbol_filter and symbol.upper() not in symbol_filter:
            continue

        if account in accounts:
            accounts[account].append(trade)
        else:
            accounts[account] = [trade,]

        if symbol in symbols:
            symbols[symbol].append(trade)
        else:
            symbols[symbol] = [trade,]

        if trade_id in trades_by_stock:
            sys.stderr.write('Duplicate trade\n')
            sys.exit(2)
        else:
            trades_by_stock[trade_id] = trade

    #pprint.pprint(all_trades)
    #pprint.pprint(accounts)
    #pprint.pprint(symbols)
    #pprint.pprint(trades_by_stock)
    #pprint.pprint(trades_by_date)

    total_money = 0
    total_trades = 0
    how_many_complete_positions = 0
    marker_counter = 0

    for symbol in symbols:

        trades = [trade
                  for trade in trades_by_date
                  if trade['symbol'] == symbol]

        trades.reverse() # most recent last

        how_many_stocks = 0
        money = 0

        stock_trades = []
        for trade in trades:
            trade_date = trade['trade-date']
            if not in_daterange(date_filter, trade_date):
                continue

            stock_trades.append(trade)
            quantity = trade['quantity']
            if trade['action'] == 'Buy':
                how_many_stocks += quantity
            else:
                how_many_stocks -= quantity

        trades.reverse() # most recent first
        if how_many_stocks != 0:
            for trade in trades:
                if date_filter:
                    if date_filter[0]:
                        trade_date = trade['trade-date']
                        if trade_date >= date_filter[0]:
                            continue
                #print('--- carry over from last year =%s stocks' % how_many_stocks)
                tmp = [trade]
                tmp.extend(stock_trades)
                stock_trades = tmp
                quantity = trade['quantity']
                if trade['action'] == 'Buy':
                    how_many_stocks += quantity
                else:
                    how_many_stocks -= quantity
                if how_many_stocks == 0:
                    break

        for trade in stock_trades:
            money += trade['net-amount']

        print("*** Symbol: %s, trades: %s, stocks: %s, money: %.2f" %
              (symbol, len(stock_trades), how_many_stocks, money))

        how_many_stocks = 0
        money = 0
        previous_profit = None

        print("%(marker)8s %(trade_id)-25s %(trade_date)12s %(action)-12s %(quantity)-10s %(price)-10s %(net_amount)-10s %(how_many_stocks)-10s %(money)-12s %(deltaMoney)-12s %(totalMoney)-12s" %
         {"marker":"Mark",
          "trade_id":"TradeId",
          "trade_date":"Date",
          "action":"Action",
          "quantity":"Quantity",
          "price":"Price",
          "net_amount":"Net",
          "how_many_stocks":"Stocks",
          "money":"Money",
          "deltaMoney":"Delta",
          "totalMoney":"Total"})

        totalMoney = 0
        for trade in stock_trades:
            trade_id = trade['trade-id']
            trade_date = trade['trade-date']
            action = trade['action']
            quantity = trade['quantity']
            price = trade['price']
            net_amount = trade['net-amount']

            money += net_amount

            if action == 'Buy':
                how_many_stocks += quantity
            else:
                how_many_stocks -= quantity

            total_trades += 1
            if how_many_stocks:
                marker = ' '
                deltaMoney = 0
            else:
                marker_counter += 1
                marker = ('%d>' % marker_counter)
                how_many_complete_positions += 1
                if previous_profit is None:
                    deltaMoney = money
                    previous_profit = money
                else:
                    deltaMoney = money - previous_profit
                    previous_profit = money
            totalMoney += deltaMoney
            print("%8s %-25s %12s %-12s %-10s %-10s %-10s %-10s %-10.2f %-10.2f %-10.2f" %
                  (marker,
                   trade_id,
                   trade_date,
                   action,
                   quantity,
                   price,
                   net_amount,
                   how_many_stocks,
                   money,
                   deltaMoney,
                   totalMoney))

        if how_many_stocks == 0:
            total_money += money

    print("Accounts: %s" % len(accounts))
    for acc in accounts:
        print("  %s" % acc)
    print("Net Profit or Loss: %.2f" % (total_money,))
    print("Total Trades: %s" % total_trades)
    print("Fully Completed Positions: %s" % how_many_complete_positions)

def commission(fname, symbol_filter, date_filter):
    if type(fname) == list:
        fname = fname[0]

    all_trades = read_trades_from_json(fname)
    total_commission = 0
    total_sec_fee = 0
    total_interest = 0

    # all_trades = list of dictionaries
    for trade in all_trades:
        trade_date = trade['trade-date']
        if not in_daterange(date_filter, trade_date):
            continue

        # if symbol_filter is set and symbol_filter not equal to current symbol - skip
        symbol = trade['symbol']
        if symbol_filter and symbol.upper() not in symbol_filter:
            continue

        total_commission += trade['commission']
        total_sec_fee += trade['fee-amount']
        total_interest += trade['interest-amount']

    print("Total commission: %.2f" % (total_commission,))
    print("Total SEC fees  : %.2f" % (total_sec_fee,))
    print("Total interest  : %.2f" % (total_interest,))

def dump_trades(inputFile, symbol_filter, date_filter):
    if type(inputFile) == list:
        fname = inputFile[0]

    all_trades = read_trades_from_json(inputFile)
    all_trades = list(sorted(all_trades,
                             key=lambda x: x['trade-date']))

    with Table([Table.Formats['date'],
                Table.Formats['symbol'],
                Table.Formats['action'],
                Table.Formats['count'],
                Table.Formats['state']]) as t:
        holdings = {}
        for trade in all_trades:
            trade_date = trade["trade-date"]

            if not in_daterange(date_filter, trade_date):
                continue

            symbol = trade["symbol"]
            if symbol_filter and symbol.upper() not in symbol_filter:
                continue

            action = trade["action"]
            quantity = int(trade["quantity"])

            if symbol not in holdings:
                holdings[symbol] = 0
            count = holdings[symbol]

            if action.upper() == 'BUY':
                count = count + quantity
            else:
                count = count - quantity

            if count == 0:
                del holdings[symbol]
            else:
                holdings[symbol] = count

            state = []
            for sym in sorted(holdings.keys()):
                count = holdings[sym]
                state.append("%s(%d)" % (sym, count))
            state = ','.join(state)

            t(trade_date, symbol, action, quantity, state)

def merge_csv_file(outputFile, inputFiles):
    '''Reads input CSV files, merges results and writes the output JSON file'''
    all_trades = []
    unique_trades = {}

    if type(inputFiles) == str:
        all_trades = read_csv_file(inputFiles)
    if type(inputFiles) == list or type(inputFiles) == tuple:
        for inputFile in inputFiles:
            all_trades.extend(read_csv_file(inputFile))

    print('Total trades: %d' % (len(all_trades)))

    for trade in all_trades:
        trade_id = trade['trade-id']
        if trade_id in unique_trades:
            if (trade['symbol'] != unique_trades[trade_id]['symbol'] or
                trade['trade-date'] != unique_trades[trade_id]['trade-date']):
                print('Duplicate trade in %s: %s [%s on %s] (seen in %s [%s on %s])' %
                    (trade['file-name'],
                     trade_id,
                     trade['symbol'],
                     trade['trade-date'],
                     unique_trades[trade_id]['file-name'],
                     unique_trades[trade_id]['symbol'],
                     unique_trades[trade_id]['trade-date']))
        else:
            unique_trades[trade_id] = trade

    #print('Unique trades: %d' % (len(unique_trades)))
    unique_trades = [unique_trades[trade_id] for trade_id in unique_trades.keys()]
    #print('Unique trades: %d' % (len(unique_trades)))
    unique_trades = list(sorted(unique_trades,
                                key=lambda x: x['trade-date']))

    convert_datetime(unique_trades, ('trade-date', 'settlement-date'))

    print('Unique trades: %d' % (len(unique_trades)))
    trades = json.dumps(unique_trades,
                        indent=2,
                        ensure_ascii=True,
                        sort_keys=True,
                        separators=(',', ':'))
    open(outputFile, 'w').write(trades)

def read_statements(inputFile):
    data = json.loads(open(inputFile, 'r').read())
    data = convert_datetime(data, ('start-date', 'end-date', 'settlement-date'))
    return data

def statements(fileName, date_filter):
    d = read_statements(fileName)

    all_deposits = []
    all_withdrawals = []
    all_interest = []
    all_dividents = []
    all_fees = []

    total_deposit = {}
    total_withdrawal = {}
    total_interest = {}
    total_dividents = {}
    total_fees = {}

    all_accounts = {}
    accounts = d['accounts']
    currencies = {}

    print("Accounts:")
    with Table([Table.Formats['name'],
                Table.Formats['description'],
                Table.Formats['currency']]) as t:
        for account in accounts:
            name = account['name']
            description = account['description']
            currency = account['currency']
            all_accounts[name] = currency
            t(name, description, currency)

    statements = d['statements']
    for year in [str(x) for x in sorted([int(x) for x in statements.keys()])]:
        data = statements[year]
        for period in data:

            if 'deposits' in period:
                deposits = period['deposits']
                for deposit in deposits:
                    settlement_date = deposit['settlement-date']
                    if not in_daterange(date_filter, settlement_date):
                        continue
                    account = deposit['account']
                    amount = deposit['amount']

                    if account in total_deposit:
                        total_deposit[account] = total_deposit[account] + amount
                    else:
                        total_deposit[account] = amount

                    all_deposits.append({'account':account,
                                         'amount':amount,
                                         'settlement-date':settlement_date,
                                         'total':total_deposit[account]})

            if 'withdrawals' in period:
                withdrawals = period['withdrawals']
                for withdrawal in withdrawals:
                    settlement_date = withdrawal['settlement-date']
                    if not in_daterange(date_filter, settlement_date):
                        continue
                    account = withdrawal['account']
                    amount = withdrawal['amount']

                    if account in total_withdrawal:
                        total_withdrawal[account] = total_withdrawal[account] + amount
                    else:
                        total_withdrawal[account] = amount

                    all_withdrawals.append({'account':account,
                                            'amount':amount,
                                            'settlement-date':settlement_date,
                                            'total':total_withdrawal[account]})

            if 'interest' in period:
                interests = period['interest']
                for interest in interests:
                    settlement_date = interest['settlement-date']
                    if not in_daterange(date_filter, settlement_date):
                        continue
                    account = interest['account']
                    amount = interest['amount']
                    description = interest['description']

                    if account in total_interest:
                        total_interest[account] = total_interest[account] + amount
                    else:
                        total_interest[account] = amount

                    all_interest.append({'account':account,
                                         'amount':amount,
                                         'description':description,
                                         'settlement-date':settlement_date,
                                         'total':total_interest[account]})

            if 'fees' in period:
                fees = period['fees']
                for fee in fees:
                    settlement_date = fee['settlement-date']
                    if not in_daterange(date_filter, settlement_date):
                        continue
                    account = fee['account']
                    amount = fee['amount']
                    description = fee['description']

                    if account in total_fees:
                        total_fees[account] = total_fees[account] + amount
                    else:
                        total_fees[account] = amount

                    all_fees.append({'account':account,
                                     'amount':amount,
                                     'description':description,
                                     'settlement-date':settlement_date,
                                     'total':total_fees[account]})

            if 'dividents' in period:
                dividents = period['dividents']
                for divident in dividents:
                    settlement_date = divident['settlement-date']
                    if not in_daterange(date_filter, settlement_date):
                        continue
                    account = divident['account']
                    amount = divident['amount']
                    description = divident['description']

                    if account in total_dividents:
                        total_dividents[account] = total_dividents[account] + amount
                    else:
                        total_dividents[account] = amount

                    all_dividents.append({'account':account,
                                          'amount':amount,
                                          'description':description,
                                          'settlement-date':settlement_date,
                                          'total':total_dividents[account]})


    all_deposits = sorted(all_deposits, key=lambda x: x['settlement-date'])
    all_withdrawals = sorted(all_withdrawals, key=lambda x: x['settlement-date'])
    all_interest = sorted(all_interest, key=lambda x: x['settlement-date'])
    all_dividents = sorted(all_dividents, key=lambda x: x['settlement-date'])
    all_fees = sorted(all_fees, key=lambda x: x['settlement-date'])

    print()
    print("Deposits:")
    with Table([Table.Formats['date'],
                Table.Formats['amount'],
                Table.Formats['total'],
                Table.Formats['currency'],
                Table.Formats['account']]) as t:
        for item in all_deposits:
            t(item['settlement-date'],
              item['amount'],
              item['total'],
              all_accounts[item['account']],
              item['account'])

    print()
    print("Withdrawals:")
    with Table([Table.Formats['date'],
                Table.Formats['amount'],
                Table.Formats['total'],
                Table.Formats['currency'],
                Table.Formats['account']]) as t:
        for item in all_withdrawals:
            t(item['settlement-date'],
              item['amount'],
              item['total'],
              all_accounts[item['account']],
              item['account'])

    print()
    print("Dividents:")
    with Table([Table.Formats['date'],
                Table.Formats['amount'],
                Table.Formats['total'],
                Table.Formats['currency'],
                Table.Formats['description'],
                Table.Formats['account']]) as t:
        for item in all_dividents:
            t(item['settlement-date'],
              item['amount'],
              item['total'],
              all_accounts[item['account']],
              item['description'],
              item['account'])

    print()
    print("Interest:")
    with Table([Table.Formats['date'],
                Table.Formats['amount'],
                Table.Formats['total'],
                Table.Formats['currency'],
                Table.Formats['description'],
                Table.Formats['account']]) as t:
        for item in all_interest:
            t(item['settlement-date'],
              item['amount'],
              item['total'],
              all_accounts[item['account']],
              item['description'],
              item['account'])

    print()
    print("Fees:")
    with Table([Table.Formats['date'],
                Table.Formats['amount'],
                Table.Formats['total'],
                Table.Formats['currency'],
                Table.Formats['description'],
                Table.Formats['account']]) as t:
        for item in all_fees:
            t(item['settlement-date'],
              item['amount'],
              item['total'],
              all_accounts[item['account']],
              item['description'],
              item['account'])

    print()
    print("Deposited Per Account:")
    with Table([Table.Formats['account'],
                Table.Formats['amount'],
                Table.Formats['currency']]) as t:
        for account, amount in total_deposit.items():
            currency = all_accounts[account]
            t(account, amount, currency)
            if currency in currencies:
                currencies[currency] = currencies[currency] + amount
            else:
                currencies[currency] = amount

    print()
    print("Withdrawn Per Account:")
    with Table([Table.Formats['account'],
                Table.Formats['amount'],
                Table.Formats['currency']]) as t:
        for account, amount in total_withdrawal.items():
            currency = all_accounts[account]
            t(account, amount, currency)
            if currency in currencies:
                currencies[currency] = currencies[currency] + amount
            else:
                currencies[currency] = amount


    print()
    print("Dividents Per Account:")
    with Table([Table.Formats['account'],
                Table.Formats['amount'],
                Table.Formats['currency']]) as t:
        for account, amount in total_dividents.items():
            currency = all_accounts[account]
            t(account, amount, currency)

    print()
    print("Interest Per Account:")
    with Table([Table.Formats['account'],
                Table.Formats['amount'],
                Table.Formats['currency']]) as t:
        for account, amount in total_interest.items():
            currency = all_accounts[account]
            t(account, amount, currency)

    print()
    print("Fees Per Account:")
    with Table([Table.Formats['account'],
                Table.Formats['amount'],
                Table.Formats['currency']]) as t:
        for account, amount in total_fees.items():
            currency = all_accounts[account]
            t(account, amount, currency)

    print()
    print("Total Cash (Deposited - Withdrawn):")
    with Table([Table.Formats['currency'],
                Table.Formats['amount']]) as t:
        for currency, amount in currencies.items():
            t(currency, amount)


def usageAndExit(errorCode = 2):
    print("""
Usage: %s [command]
Where [command] is:
    -c | --calculate    calculate total profit (works with filters)
    -d | --dump         dump all transactions
    -f | --commission   calculate total commission (works with filters)
    -h | --help         show help and exit
    -m | --merge        merge input CSV files into one and output to the output JSON file
         --state        calculate statement information

Options are:
    -b | --begin        set start date filter. Date is in format YYYY/MM/DD.
    -e | --end          set end date filter. Date is in format YYYY/MM/DD.
    -o | --output       set output file name. The output file is in JSON format.
    -s | --symbol       set symbol filter. Either one ticker or
    -v | --verbose      be verbose in output.
""" % (os.path.basename(sys.argv[0])))
    sys.exit(errorCode)

def main():
    locale.setlocale(locale.LC_ALL, '')
    try:
        opts, args = getopt.getopt(sys.argv[1:],
                                   "b:cde:fhmo:s:v",
                                   ["begin=",
                                    "calculate",
                                    "commission",
                                    "dump",
                                    "end=",
                                    "help",
                                    "merge",
                                    "output=",
                                    "state",
                                    "symbol=",
                                    "verbose",
                                    "withdrawals"
                                    ])
    except getopt.GetoptError as err:
        usageAndExit(2)

    verbose = False
    symbol_filter = []
    date_filter = [None, None]
    input_file = None
    output_file = None
    mode = ''

    for opt, argument in opts:
        if opt in ("-v", "--verbose"):  verbose = True
        elif opt in ("-h", "--help"):   usageAndExit(0)
        elif opt in ("-o", "--output"): output_file = args if args else 'all.json'
        elif opt in ("-m", "--merge"):  mode, input_file = 'merge', args
        elif opt in ("-s", "--symbol"): symbol_filter.extend(parse_symbols(argument))
        elif opt in ("-b", "--begin"):  date_filter = [string_to_datetime(argument), date_filter[1]]
        elif opt in ("-e", "--end"):    date_filter = [date_filter[0], string_to_datetime(argument)]
        elif opt in ("-c", "--calculate"): mode, input_file = 'calculate', (args if args else 'all.json')
        elif opt in ("-f", "--commission"): mode, input_file = 'commission', (args if args else 'all.json')
        elif opt in ("--state"): mode, input_file = 'state', (args if args else 'statements.json')
        elif opt in ("-d", "--dump"): mode, input_file = 'dump', (args if args else 'all.json')
        else:
            usageAndExit(2)

    print_symbol_filter(symbol_filter)
    print_date_filter(date_filter)

    if mode == 'merge':
        merge_csv_file(output_file, args)
    elif mode == 'calculate':
        calculate(input_file, symbol_filter, date_filter)
    elif mode == 'commission':
        commission(input_file, symbol_filter, date_filter)
    elif mode == 'dump':
        dump_trades(input_file, symbol_filter, date_filter)
    elif mode == 'state':
        statements(input_file, date_filter)
    else:
        usageAndExit(2)

if __name__ == "__main__":
    main()

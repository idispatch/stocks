@echo off
@echo BASE_DIR=         %BASE_DIR%
@echo BASE_DIR_REPLACED=%BASE_DIR_REPLACED%
@echo LATEST_JRE=       %LATEST_JRE%
@echo QNX_HOST=         %QNX_HOST%
@echo QNX_TARGET=       %QNX_TARGET%
@echo QNX_CONFIGURATION=%QNX_CONFIGURATION%
@echo MAKEFLAGS=        %MAKEFLAGS%
@echo QDE=              %QDE%
